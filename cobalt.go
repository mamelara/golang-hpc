package hpc

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

type CobaltJob struct {
	*Job
	batchCommand string
	args         []string
	out          []string
	statusCmd    string
	jobID        string
	sleepTime    time.Duration
}

func (j CobaltJob) New(job *Job) (CobaltJob, error) {
	//Create script Check for Errors
	script, err := job.buildScript()
	if err != nil {
		return CobaltJob{}, err
	}

	outputScriptPath, err := job.mkTempFile(job, "cobalt_out-*.log")
	if err != nil {
		job.PrintWarning("Failed to create cobalt_out-*.log file")
		return CobaltJob{}, err
	}

	errorScriptPath, err := job.mkTempFile(job, "cobalt_err-*.log")
	if err != nil {
		job.PrintWarning("Failed to create cobalt_err-*.log file")
		return CobaltJob{}, err
	}

	files := []string{outputScriptPath, errorScriptPath}
	execArgs := []string{"-o", outputScriptPath, "-e", errorScriptPath}

	//Handle Native Specs
	if len(job.NativeSpecs) != 0 {
		//Defines an array of illegal arguments which will not be passed in as native specifications
		illegalArguments := []string{"-o", "--output", "-e", "--error"}
		warnings := j.Job.CheckIllegalParams(job.NativeSpecs, illegalArguments)
		for i := range warnings {
			job.PrintWarning(warnings[i])
		}
		execArgs = append(execArgs, job.NativeSpecs...)
	}

	execArgs = append(execArgs, script)

	return CobaltJob{job, "qsub", execArgs, files, "qstat", "", 30 * time.Second}, nil
}

func (j *CobaltJob) RunJob() (builderr error, syserr error) {
	cmd := j.Job.setUID()
	qsubStdin := fmt.Sprintf("%s %s", j.batchCommand, strings.Join(j.args, " "))
	cmd.Stdin = bytes.NewBufferString(qsubStdin)

	//Handle stdout and stderr
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	//Run the command, check for errors
	syserr = cmd.Run()
	if syserr != nil {
		logrus.WithFields(logrus.Fields{
			"job":   j.ParentJobID,
			"qsub":  qsubStdin,
			"error": syserr,
		}).Warn("Failed qsub command")
		j.PrintWarning(fmt.Sprintf("Failed to request Cobalt allocation: %v", syserr))
		return
	}

	jobid, syserr := strconv.Atoi(strings.TrimSpace(stdout.String()))
	if syserr != nil {
		logrus.WithFields(logrus.Fields{
			"error": syserr,
		}).Warn("Failed to read job ID")
		j.PrintToParent(fmt.Sprintf("Failed to read job ID: %#v", syserr))
		return
	}
	j.jobID = strconv.Itoa(jobid)

	var wg sync.WaitGroup
	wg.Add(len(j.out))

	done := make(chan bool)
	for _, file := range j.out {
		logrus.WithFields(logrus.Fields{
			"job":       j.ParentJobID,
			"cobaltJob": j.jobID,
			"file":      file,
		}).Debug("Watching file")
		go func() {
			defer wg.Done()
			j.Job.tailFile(file, done)
		}()
		defer j.manageLog(file)
	}

	//Build a command to check job status
	qstatArg := fmt.Sprintf("%s %d", j.statusCmd, jobid)

	//Loop until qstat returns non-zero
	for j.qstatCmd(qstatArg) == nil {
		time.Sleep(j.sleepTime)
		logrus.WithFields(logrus.Fields{
			"job":       j.ParentJobID,
			"cobaltJob": j.jobID,
		}).Debug("Checking job status")
	}

	time.Sleep(j.sleepTime)
	close(done)
	wg.Wait()

	logrus.WithFields(logrus.Fields{
		"job":       j.ParentJobID,
		"cobaltJob": j.jobID,
	}).Debug("Cobalt job completed")

	// The folder location for all records relating to jobs ran (@ALCF) is static
	// TODO: allow admin configuration via appropriate toml
	exitStatus, syserr := cobaltExitStatus("/var/log/pbs", j.jobID)
	if syserr != nil {
		return
	} else if exitStatus != "0" {
		return fmt.Errorf("cobalt job %s, final Exit_status: %s", j.jobID, exitStatus), nil
	}

	return
}

// KillJob build command to kill job (qdel), j.jobID must be assigned.
// Returns system error if encountered.
func (j *CobaltJob) KillJob() (err error) {
	cmd := j.Job.setUID()
	cmd.Stdin = bytes.NewBufferString(fmt.Sprintf("qdel %s", j.jobID))
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("cannot kill job %s: %v", j.jobID, err)
	}
	return
}

// qstatCmd recreates a cmd so that qstat can be repeatedly called, return error
// when non-zero exit status is encountered.
func (j *CobaltJob) qstatCmd(stdin string) error {
	cmd := j.Job.setUID()
	cmd.Stdin = strings.NewReader(stdin)
	return cmd.Run()
}

// cobaltExitStatus checks accounting logs created by date the job completed (yyyymmdd) using
// the location provided (logs). Returning the exit status of the job id if found, system
// error returned if encountered.
func cobaltExitStatus(logs, id string) (es string, err error) {
	t := time.Now()
	y := t.AddDate(0, 0, -1)
	es, err = checkForExitStatus(fmt.Sprintf("%s/%d%02d%02d", logs, t.Year(), t.Month(), t.Day()), id)
	if err != nil { // regardless of error check the previous days log just in case
		es, err = checkForExitStatus(fmt.Sprintf("%s/%d%02d%02d", logs, y.Year(), y.Month(), y.Day()), id)
	}
	return
}

// checkForExitStatus checks the contents of the file (name) using the id. If a matching
// 'Exit_status=?' is found it is returned. A system error is returned if encountered.
func checkForExitStatus(name, id string) (string, error) {
	f, err := os.OpenFile(name, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return "", fmt.Errorf("unable to open file: %v", err)
	}
	defer f.Close()

	a := regexp.MustCompile(`Exit_status=(\d+)`)
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		line := sc.Text()
		if strings.Contains(line, fmt.Sprintf(";%s;", id)) {
			matches := a.FindStringSubmatch(line)
			if len(matches) != 2 {
				continue
			}
			return matches[1], nil
		}
	}
	if err := sc.Err(); err != nil {
		return "", fmt.Errorf("scanner error: %v", err)
	}

	return "", fmt.Errorf("no Exit_status found for %v", id)
}
