package hpc

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/radovskyb/watcher"
	"github.com/sirupsen/logrus"
)

type Job struct {
	ScriptContents  string
	NativeSpecs     []string
	UID             int
	GID             int
	OutputScriptPth string
	PrintToParent   func(string)
	PrintWarning    func(string)
	SpecifiedBatch  string // User specified batch system to be used (automated if nil)
	ParentJobID     string // Parent's unique job id (e.g. CI_JOB_ID)
	RetainLogs      bool   // If scheduler logs should be retained in the directory they were created
	CopyLogsDir     string // Copy scheduler logs (by user) into target directory
	curUser         *user.User
	currentLSF      LSFJob
	currentSLURM    SlurmJob
	currentCOBALT   CobaltJob
}

// timeout for largest possible delay to expect from NFS servers for file data
// This is a Duration string.
const NFSTimeout = "1m"

// printed at the end of log files so we know when to stop tailing.
const JobCompletionString = "Batch job completed successfully."

//This function exists to help the native spec system
func Contains(illegalArgs []string, elementToTest string) bool {
	for _, illegalArg := range illegalArgs {
		if elementToTest == illegalArg {
			return true
		}
		if strings.Contains(elementToTest, illegalArg) {
			return true
		}
	}
	return false
}

// Generate warning (string) if specified arguments are encountered
func (j *Job) CheckIllegalParams(input []string, illegalParams []string) []string {
	var warnings []string
	for i := range input {
		for k := range illegalParams {
			if strings.HasPrefix(input[i], illegalParams[k]) {
				warnings = append(warnings, fmt.Sprint(input[i], " argument may cause unwanted results"))
			}
		}
	}
	return warnings
}

//Initial func run. Gets batch system and calls the corresponding run
func (j *Job) Run() (builderr error, syserr error) {
	sched, err := j.findScheduler()
	if err != nil {
		return nil, err
	}

	j.curUser, err = user.LookupId(strconv.Itoa(j.UID))
	if err != nil || j.curUser == nil {
		return nil, fmt.Errorf("user lookup failed: %v", err)
	}

	j.updateTargetProfile()
	if sched == "slurm" {
		l := new(SlurmJob)
		j.currentSLURM, err = l.New(j)
		if err != nil {
			return nil, err
		}
		return j.currentSLURM.RunJob()
	} else if sched == "lsf" {
		l := new(LSFJob)
		j.currentLSF, err = l.New(j)
		if err != nil {
			return nil, err
		}
		return j.currentLSF.RunJob()
	} else if sched == "cobalt" {
		l := new(CobaltJob)
		j.currentCOBALT, err = l.New(j)
		if err != nil {
			return nil, err
		}
		return j.currentCOBALT.RunJob()
	}

	return nil, fmt.Errorf("no batch system found")
}

func (j *Job) Kill() {
	// TODO: implement proper interface
	var err error
	sched, _ := j.findScheduler()
	if sched == "slurm" {
		err = j.currentSLURM.KillJob()
	} else if sched == "lsf" {
		err = j.currentLSF.KillJob()
	} else if sched == "cobalt" {
		err = j.currentCOBALT.KillJob()
	}

	if err != nil {
		logrus.WithFields(logrus.Fields{
			"job":   j.ParentJobID,
			"error": err,
		}).Debug("Scheduled job not canceled")
	}
}

// updateTargetProfile ensures the job script loads the appropriate
// profiles based upon user/admin settings. TODO: expand logic to better
// address user level variables
func (j *Job) updateTargetProfile() {
	script := `#!/bin/bash --login

`
	j.ScriptContents = script + j.ScriptContents
}

// buildScript creates a script container the j.ScriptContents and
// returns the absolute path. System error returned if encountered.
func (j *Job) buildScript() (string, error) {
	uniqueID := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	if err := os.MkdirAll(fmt.Sprint(j.OutputScriptPth, "/scripts"), 0740); err != nil {
		return "", err
	}
	if err := os.Chown(fmt.Sprint(j.OutputScriptPth, "/scripts"), j.UID, j.GID); err != nil {
		return "", err
	}

	batchScriptFull := j.OutputScriptPth + "/scripts/" + j.ParentJobID + "_" + uniqueID + ".bash"
	batchScript, err := os.Create(batchScriptFull)
	if err != nil {
		return "", fmt.Errorf("unable to create job script: %v", err)
	}
	if _, err := batchScript.WriteString(j.ScriptContents); err != nil {
		return "", fmt.Errorf("unable to write to job script (%s): %v", batchScriptFull, err)
	}

	if err := os.Chmod(batchScriptFull, 0750); err != nil {
		return "", err
	}
	if err := os.Chown(batchScriptFull, j.UID, j.GID); err != nil {
		return "", err
	}
	batchScript.Close()

	return batchScriptFull, nil
}

func (j *Job) tailPipe(pipe io.ReadCloser) {
	scanner := bufio.NewScanner(pipe)
	for scanner.Scan() {
		j.PrintToParent(scanner.Text())
	}
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func (j *Job) tailFile(fileName string, done chan bool) {
	// wait for the file to exist if it does not already
	sleepTime, _ := time.ParseDuration("30ms")
	for !fileExists(fileName) {
		time.Sleep(sleepTime)
	}

	// now tail it once it's been created
	w := watcher.New()

	w.SetMaxEvents(1)
	w.FilterOps(watcher.Write)

	file, ferr := os.Open(fileName)
	if ferr != nil {
		logrus.Error(ferr)
		return
	}
	defer file.Close()

	go func() {
		defer w.Close()
		finished := false

		for {
			scanner := bufio.NewScanner(file)
			for scanner.Scan() {
				text := scanner.Text()
				j.PrintToParent(scanner.Text())

				// this line is written at the end of the submitted job script
				// to say that the output is done in a synchronous way
				if text == JobCompletionString {
					finished = true
				}
			}

			if finished {
				return
			}

			file.Seek(0, os.SEEK_CUR)

			select {
			case <-w.Event:
				continue
			case err := <-w.Error:
				logrus.WithFields(logrus.Fields{
					"job":      j.ParentJobID,
					"filename": fileName,
					"error":    err,
				}).Warn("File watcher error")
				return
			case <-w.Closed:
				logrus.WithFields(logrus.Fields{
					"job":      j.ParentJobID,
					"filename": fileName,
				}).Warn("File watcher unexpected closed")
				finished = true
			case <-done:
				// caller can kill the tail routine by closing the done channel
				finished = true
			}
		}
	}()

	// Watch this folder for changes.
	if err := w.Add(fileName); err != nil {
		return
	}

	// Start the watching process - it'll check for changes every 5s.
	if err := w.Start(time.Second * 5); err != nil {
		return
	}

}

func (j *Job) mkTempFile(job *Job, template string) (out string, err error) {
	file, err := ioutil.TempFile(job.OutputScriptPth, template)
	if err != nil {
		return "", err
	}

	fileName := file.Name()

	if err = os.Chown(fileName, job.UID, job.GID); err != nil {
		logrus.WithFields(logrus.Fields{
			"job":      j.ParentJobID,
			"filename": fileName,
			"uid":      job.UID,
			"error":    err,
		}).Error("System chown operation failed")
		return "", err
	}

	return fileName, nil
}

// setUID generate a valid GoLang cmd (bash shell) while accounting for runner's setuid requirements.
func (j *Job) setUID() (cmd *exec.Cmd) {
	cmd = exec.Command("env", "-i", fmt.Sprintf("HOME=%s", j.curUser.HomeDir), "bash", "--login")

	if os.Geteuid() == 0 {
		//Assign setUID information and env. vars
		cmd.SysProcAttr = &syscall.SysProcAttr{}
		cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(j.UID), Gid: uint32(j.GID)}
	}

	// Provide a temporary minimal environment for subsequent commands
	cmd.Env = []string{
		fmt.Sprintf("LOGNAME=%s", j.curUser.Username),
		fmt.Sprintf("USER=%s", j.curUser.Username),
		fmt.Sprintf("HOME=%s", j.curUser.HomeDir),
		"PATH=/usr/bin:/bin",
	}

	logrus.WithFields(logrus.Fields{
		"job": j.ParentJobID,
		"uid": j.UID,
		"cmd": cmd,
	}).Debug("Successfully built SetUID command")

	return
}

// findScheduler checks for a userdefined batch system and if one
// is not defined then attempts to locate a valid one on the system's PATH
func (j *Job) findScheduler() (string, error) {
	if j.SpecifiedBatch != "" {
		return checkSupportSchedulers(j.SpecifiedBatch)
	}
	_, err := exec.LookPath("sbatch")
	if err == nil {
		return "slurm", nil
	}
	_, err = exec.LookPath("bsub")
	if err == nil {
		return "lsf", nil
	}
	_, err = exec.LookPath("qsub")
	if err == nil {
		_, err = exec.LookPath("qstat")
		if err == nil {
			return "cobalt", nil
		}
		return "", fmt.Errorf("Cobalt detected but can't monitor (found qsub but no qstat)")
	}
	return "", fmt.Errorf("No batch system found")
}

// checkSupportSchedulers verifies that the user defined batch system is currently
// supported and return the appropriate internal system identify
func checkSupportSchedulers(sys string) (string, error) {
	supported := map[string]string{
		"slurm":  "slurm",
		"lsf":    "lsf",
		"cobalt": "cobalt",
	}
	if val, ok := supported[strings.ToLower(sys)]; ok {
		return val, nil
	}

	return "", fmt.Errorf("No supported batch system for: %s", sys)
}

// ManageLog ensure post job logic for the scheduler created log file is properly executed.
func (j *Job) manageLog(file string) {
	if !j.RetainLogs {
		defer os.Remove(file)
	}

	// copying the log file will be managed by the user's shell, thusly ACS is observed
	if j.CopyLogsDir != "" {
		if !strings.HasSuffix(j.CopyLogsDir, "/") {
			j.CopyLogsDir = j.CopyLogsDir + "/"
		}
		cmd := j.setUID()
		cmd.Stdin = bytes.NewBufferString("mkdir -p " + j.CopyLogsDir + " && cp " + file + " " + j.CopyLogsDir)
		ret, err := cmd.CombinedOutput()
		if err != nil {
			j.PrintWarning(fmt.Sprintf("error encountered while attempting to copy scheduler logs: " + string(ret)))
		}
	}
}
