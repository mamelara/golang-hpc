package hpc

import (
	"bytes"
	"errors"
	"fmt"
	"path"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

// SlurmJob represents a slurm job's metadata.
// Job information about current slurm job such as UID,GID, etc.
// batchCommand used for submitting via batch.
// args arguments use for submitting to a batch job.
// out stdout of the Slurm job.
// statusCmd status of the current Slurm job.
// exitCodeCmd exit code of the current Slurm job.
// jobId ID for the current Slurm job.
// sleepTime how long to sleep between polling job status.
type SlurmJob struct {
	*Job
	batchCommand string
	args         []string
	out          []string
	statusCmd    string
	exitCodeCmd  string
	jobID        string
	sleepTime    time.Duration
}

// New constructs a new Slurm object for submitting jobs and collecting information about slurm jobs.
// It returns a SlurmJob struct with assigned attributes otherwise it returns an error if it cannot
// create a new job.
func (j SlurmJob) New(job *Job) (SlurmJob, error) {
	//Create script Check for Errors
	script, err := job.buildScript()
	if err != nil {
		return SlurmJob{}, err
	}

	//Get output script paths -- initially, use %j to have SLURM substitute the
	// job id, then fix it later once we know what the job id is
	outputScriptPath := path.Join(job.OutputScriptPth, "slurm-out-%j.log")
	execArgs := []string{"-o", outputScriptPath}

	//Handle Native Specs
	if len(job.NativeSpecs) != 0 {
		//Defines an array of illegal arguments which will not be passed in as native specifications
		illegalArguments := []string{"-o", "--output", "-e", "--error"}
		warnings := j.Job.CheckIllegalParams(job.NativeSpecs, illegalArguments)
		for i := range warnings {
			job.PrintWarning(warnings[i])
		}
		execArgs = append(execArgs, job.NativeSpecs...)
	}

	files := []string{outputScriptPath}
	execArgs = append(execArgs, script)

	return SlurmJob{job, "sbatch", execArgs, files, "squeue", "sacct", "", 30 * time.Second}, nil
}

// RunJob runs the current slurm job and monitors progress by using sacct at intervals
// of 30 seconds. It watches slurm log files until they are completed and polls sacct
// for job status. If the job returns a COMPLETED/COMPLETING status and the job also stops
// writing to it's log file, the job will exit successfully otherwise it will return an error either
// stemming from the user's script (builderr) or a system error (syserr).
func (j *SlurmJob) RunJob() (builderr error, syserr error) {
	cmd := j.Job.setUID()
	sbatchStdin := fmt.Sprintf("%s %s", j.batchCommand, strings.Join(j.args, " "))
	cmd.Stdin = bytes.NewBufferString(sbatchStdin)

	// execute sbatch
	stdOut, err := cmd.CombinedOutput()
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"job":    j.ParentJobID,
			"sbatch": sbatchStdin,
			"error":  err,
		}).Warn("Failed sbatch command")
		j.PrintWarning(fmt.Sprintf("Failed to request Slurm allocation: %v", err))
		return nil, fmt.Errorf(string(stdOut))
	}

	j.PrintToParent(string(stdOut))
	j.jobID, syserr = sbatchJobID(string(stdOut))
	if syserr != nil {
		return
	}

	// fix up %j's in the filenames in j.out (%% is the escape for %)
	fixupJob := regexp.MustCompile(`(^|[^%])%j`)
	for i, file := range j.out {
		j.out[i] = fixupJob.ReplaceAllString(file, "${1}"+j.jobID)
	}

	done := make(chan bool)

	// We'll wait for ALL the files to finish, OR squeue to report the job done
	var wg sync.WaitGroup
	wg.Add(len(j.out))

	// Tail the job's output until we find JobCompletionString, which
	// tells us we're done.
	//
	// If the job terminates abnormally, we MAY not see
	// JobCompletionString in the output file, so tailFile can also be
	// killed by closing the `done` channel -- see below.
	for _, file := range j.out {
		logrus.WithFields(logrus.Fields{
			"job":      j.ParentJobID,
			"slurmJob": j.jobID,
			"file":     file,
		}).Debug("Watching file")
		go func() {
			defer wg.Done()
			j.Job.tailFile(file, done)
		}()
		defer j.manageLog(file)
	}

	// make a channel that will be closed when all files are done.
	filesDone := make(chan bool)
	go func() {
		wg.Wait()
		close(filesDone)
	}()

	jobBuilderr := make(chan error, 1)
	jobSyserr := make(chan error, 1)

	// time out after 20 minutes if we're stuck in any
	// stage.
	timeOut := time.Duration(20 * time.Minute)
	go func() {
		berr, syserr := j.obtainSacct(timeOut)
		jobSyserr <- syserr
		jobBuilderr <- berr
	}()

	// wait for one of the above channels to close and signal we're done
	var msg string
	select {
	case <-filesDone:
		logrus.Debug("All files finished writing.")
	case builderr = <-jobBuilderr:
		syserr = <-jobSyserr
	}

	logrus.WithFields(logrus.Fields{
		"job":      j.ParentJobID,
		"slurmJob": j.jobID,
		"message":  msg,
	}).Debug("Slurm job completed")

	// this kills tailFile if squeue checks finished first.
	close(done)

	logrus.WithFields(logrus.Fields{
		"jobid": j.jobID,
	}).Debug("Job completed.")
	return
}

// KillJob build command to kill job (scancel), j.jobID must be assigned.
// Returns system error if encountered.
func (j *SlurmJob) KillJob() (err error) {
	cmd := j.Job.setUID()
	cmd.Stdin = bytes.NewBufferString(fmt.Sprintf("scancel %s", j.jobID))
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("cannot kill job %s: %v", j.jobID, err)
	}
	return
}

// squeueCmd recreates a cmd so that qstat can be repeatedly called, return output from
// command and system error if encountered.
func (j *SlurmJob) squeueCmd(stdin string) ([]byte, error) {
	cmd := j.Job.setUID()
	cmd.Stdin = strings.NewReader(stdin)
	return cmd.Output()
}

// sbatchJobID parses stdout patterns from sbatch call to retrieve jobid
// returns system error if encountered
func sbatchJobID(out string) (string, error) {
	a := regexp.MustCompile(`[S-s]ubmitted\sbatch\sjob\s(\d+)`)
	matches := a.FindStringSubmatch(out)
	if len(matches) != 2 {
		return "", fmt.Errorf("unable to obtain jobID from sbatch stdout")
	}
	return matches[1], nil
}

// sacctState parses stdout from the sacct cmd to retrieve the job's state
// expected: $ sacct -n -j # --format=state
func sacctState(out string) string {
	lines := strings.Split(out, "\n")
	return strings.TrimSpace(lines[0])
}

// sacctCmd recreates a Cmd so that sacct can be repeatedly called with
// the same arguments over and over. Prevents an issue where Stdin would get
// consumed when executing th Cmd.
func (j *SlurmJob) sacctCmd(stdin string) ([]byte, error) {
	cmd := j.Job.setUID()
	cmd.Stdin = strings.NewReader(stdin)
	return cmd.Output()
}

// ObtainSacct uses Slurm's sacct program to obtain the accouting information for the job
// returns build or system error. System error is only encountered if sacct cannot be invoked
func (j *SlurmJob) obtainSacct(timeLimit time.Duration) (builderr error, syserr error) {
	// Build a command to get exit status of the Job, executed as setuid target
	// get yesterday's date in case job started in middle of the night
	yesterday := time.Now().AddDate(0, 0, -1)
	startDate := yesterday.Format("01/02/06")
	sacctArg := fmt.Sprintf("%s -n -j %s -L --format=state -S %s", j.exitCodeCmd, j.jobID, startDate)
	ret, err := j.sacctCmd(sacctArg)

	var timeout *time.Timer

	// Wait for job to complete, then get return code
Loop:
	for {
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"args":  sacctArg,
				"error": err,
			}).Debug("System Error: Cannot get output via sacct")
			return nil, fmt.Errorf("System Error: Cannot get output via sacct: %v", err)
		}

		var msg string

		switch jobState := sacctState(string(ret)); jobState {
		case "PENDING", "RUNNING":
			ret, err = j.sacctCmd(sacctArg)
			time.Sleep(j.sleepTime)
		case "COMPLETING":
			if timeout == nil {
				timeout = time.NewTimer(timeLimit)
				defer timeout.Stop()
			}
			select {
			case <-timeout.C:
				err = errors.New("stage stuck in COMPLETING and timed out")
				logrus.WithFields(logrus.Fields{
					"parentJobID": j.ParentJobID,
					"slurmJobID":  j.jobID,
					"args":        sacctArg,
					"error":       err,
				}).Warn(err)
				return nil, err
			default:
				ret, err = j.sacctCmd(sacctArg)
				time.Sleep(j.sleepTime)
			}
		case "COMPLETED":
			msg = "Job successful"
			logrus.WithFields(logrus.Fields{
				"sacct":    sacctArg,
				"error":    err,
				"jobID":    j.jobID,
				"jobState": jobState,
			}).Debug(msg)
			break Loop
		default: // Default will handle any other state that is considered an error.
			logrus.WithFields(logrus.Fields{
				"args":  sacctArg,
				"error": err,
			}).Debug("System Error: Cannot get output via sacct")
			return fmt.Errorf("slurm job %v failed, final state %s", j.jobID, jobState), nil
		}
	}
	return
}
