package hpc

import (
	"os/exec"
	"testing"
	"time"
)

func newSlurmJob(job *Job) SlurmJob {
	return SlurmJob{job, "sbatch", nil, nil, "squeue", "sacct", "", 30 * time.Second}
}

func cleanMockSlurm() {
	cmd := exec.Command("mock_slurm.py", "clean")
	_ = cmd.Run()
}

func skipIfNoScheduler(t *testing.T) {
}

func Test_sacctState(t *testing.T) {
	type args struct {
		out string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"Multi-line", args{
			out: `   TIMEOUT
 	CANCELLED`}, "TIMEOUT"},
		{"Single-line", args{out: "COMPLETED"}, "COMPLETED"},
		{"EMPTY", args{out: ""}, ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sacctState(tt.args.out); got != tt.want {
				t.Errorf("sacctState() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sbatchJobID(t *testing.T) {
	_, err := exec.LookPath("mock_slurm.py")
	if err != nil {
		t.Skip("mock_slurm.py not found")
	}
	type args struct {
		out string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"Standard return", args{out: "Submitted batch job 123\n"}, "123", false},
		{"Cluster specific return", args{out: "Submitted batch job 456789 on cluster example"}, "456789", false},
		{"Debug output", args{out: "sbatch: slurm_spank_init nodes=0\nsbatch: argc[0] is host=localhost\nsbatch: argc[1] is test=true\nSubmitted batch job 5\nsbatch: slurm_spank_exit"}, "5", false},
		{"No jobID", args{out: "Submitted batch job"}, "", true},
		{"Multiple jobID found", args{out: "TEST\nSubmitted batch job 1\nSubmitted batch job 2"}, "1", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := sbatchJobID(tt.args.out)
			if (err != nil) != tt.wantErr {
				t.Errorf("sbatchJobID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("sbatchJobID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSlurmJob_KillJob(t *testing.T) {
	_, err := exec.LookPath("mock_slurm.py")
	if err != nil {
		t.Skip("mock_slurm.py not found")
	}
	cleanMockSlurm()

	tests := []struct {
		name    string
		jobID   string
		wantErr bool
	}{
		{"Simulated error", "600", true},
		{"Standard Job", "200", false},
		{"No JobID", "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j, err := newJob()
			if err != nil {
				t.Error(err)
				return
			}
			j.currentSLURM = newSlurmJob(j)
			j.currentSLURM.jobID = tt.jobID
			if err := j.currentSLURM.KillJob(); (err != nil) != tt.wantErr {
				t.Errorf("SlurmJob.KillJob() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_obtainSacct(t *testing.T) {
	_, err := exec.LookPath("mock_slurm.py")
	if err != nil {
		t.Skip("mock_slurm.py not found")
	}

	cleanMockSlurm()

	tests := []struct {
		name    string
		jobID   string
		wantErr bool
	}{
		// successful runs
		{"PENDING state", "400", false},
		{"RUNNING state", "500", false},
		{"COMPLETED state", "100", false},
		// failed runs
		{"COMPLETING state", "700", true},
		{"Empty Return", "999", true},
		{"Non-Zero Exit", "600", true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j, err := newJob()
			if err != nil {
				t.Error(err)
				return
			}
			timeout := time.Duration(2 * time.Second)
			jobID := tt.jobID
			j.currentSLURM = newSlurmJob(j)
			j.currentSLURM.jobID = jobID

			_, syserr := j.currentSLURM.obtainSacct(timeout)
			if syserr != nil && !tt.wantErr {
				t.Errorf("SlurmJob.obtainSacct() error = %v, wantErr %v", err, tt.wantErr)
			} else if syserr == nil && tt.wantErr {
				t.Errorf("Job did not throw error")
			}
		})
	}
}
