package hpc

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"reflect"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var tmpfile *os.File

// output script location.  The %d will be expanded to the current pid,
// so that iterative runs of this test have separate storage locations.
const outputScriptPath = "/g/g21/gamblin2/runner-out/%d"

var sleepJobScript = `#!/bin/bash
id
date
sleep 60
`

func generateJobScript(script string) string {
	path, _ := os.Getwd()
	tmpfile, _ := ioutil.TempFile(path, "TestOutput.bash")
	fmt.Println(tmpfile.Name())
	tmpfile.WriteString(script)
	tmpfile.Close()
	return tmpfile.Name()
}

// write to temp file for later validation.
func PrintOut(text string) {
	tmpfile.WriteString(text + "\n")
	fmt.Printf("OUT    %s\n", text)
}

// this isn't tested right now.
func PrintErr(text string) {
	fmt.Printf("ERR    %s\n", text)
}

// newJob generates a Job struct for the current user
func newJob() (*Job, error) {
	job := new(Job)

	path, _ := os.Getwd()
	job.OutputScriptPth = path

	// get current user uid/gid
	usr, err := user.Current()
	if err != nil {
		return job, fmt.Errorf(fmt.Sprintf("Unexpected error attempting to identify the current user for the runner: %v", err))
	}
	uid, uidErr := strconv.Atoi(usr.Uid)
	gid, gidErr := strconv.Atoi(usr.Gid)
	if uidErr != nil && gidErr != nil {
		return job, fmt.Errorf(fmt.Sprintf("Unexpected error attempting to identify the current user for the runner: %v", err))
	}
	job.UID = uid
	job.GID = gid

	job.PrintToParent = PrintOut
	job.PrintWarning = PrintErr
	job.ParentJobID = "123"

	return job, nil
}

func TestJob_findScheduler(t *testing.T) {
	type fields struct {
		SpecifiedBatch string
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{"Specified scheduler - slurm", fields{SpecifiedBatch: "slurm"}, "slurm", false},
		{"Specified scheduler - lsf", fields{SpecifiedBatch: "LSF"}, "lsf", false},
		{"Specified scheduler - cobalt", fields{SpecifiedBatch: "Cobalt"}, "cobalt", false},
		{"No scheduler found", fields{SpecifiedBatch: ""}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &Job{
				SpecifiedBatch: tt.fields.SpecifiedBatch,
			}
			got, err := j.findScheduler()
			if (err != nil) != tt.wantErr {
				t.Errorf("Job.findScheduler() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Job.findScheduler() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_checkSupportSchedulers(t *testing.T) {
	type args struct {
		sys string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"Slurm", args{"slurm"}, "slurm", false},
		{"LSF", args{"LSF"}, "lsf", false},
		{"Colabt", args{"Cobalt"}, "cobalt", false},
		{"Unsupported", args{"unsupported"}, "", true},
		{"Empty", args{""}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := checkSupportSchedulers(tt.args.sys)
			if (err != nil) != tt.wantErr {
				t.Errorf("checkSupportSchedulers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("checkSupportSchedulers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJob_CheckIllegalParams(t *testing.T) {
	type args struct {
		input         []string
		illegalParams []string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{"No arguments", args{nil, nil}, nil},
		{"No illegal", args{[]string{"--alpha", "-b"}, nil}, nil},
		{"No input", args{nil, []string{"-a", "--bravo"}}, nil},
		{"Matching", args{[]string{"-a", "--charlie=1"}, []string{"--charlie"}}, []string{"--charlie=1 argument may cause unwanted results"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j, err := newJob()
			if err != nil {
				t.Error(err)
			}

			if got := j.CheckIllegalParams(tt.args.input, tt.args.illegalParams); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Job.CheckIllegalParams() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOutput(t *testing.T) {
	const outputLength = 100 // Length of the test output

	j, err := newJob()
	if err != nil {
		t.Error(err)
	}

	// set nativeSpecs for test based upon scheduler found
	var nativeSpecs []string
	sched, err := j.findScheduler()
	if err != nil {
		t.Skip(err)
	}
	switch sched {
	case "slurm":
		nativeSpecs = []string{"-N1"}
	case "lsf":
		nativeSpecs = []string{"-ppdebug", "-n", "1"}
	case "cobalt":
		nativeSpecs = []string{"-q", "pbatch", "-nnodes", "1", "-G", "guests"}
	}
	j.NativeSpecs = nativeSpecs

	// this is our test submission script
	j.ScriptContents = fmt.Sprintf(`
	for i in $(seq 1 %d); do echo $i; done
	`, outputLength)

	tmpfile, _ = ioutil.TempFile("", "TestOutput")
	defer os.Remove(tmpfile.Name())

	j.Run()
	tmpfile.Close()

	actualBytes, _ := ioutil.ReadFile(tmpfile.Name())
	actualOutput := string(actualBytes)

	var expectedOutput = "\n"
	for i := 1; i <= outputLength; i++ {
		expectedOutput += fmt.Sprintf("%d\n", i)
	}
	expectedOutput += JobCompletionString + "\n"

	// use HasSuffix to ignore anything printed from the user's shell init files.
	if !strings.HasSuffix(actualOutput, expectedOutput) {
		t.Errorf(`expected:
` + expectedOutput + `
found:
` + actualOutput + "\n")
	}
}

func TestJob_manageLog(t *testing.T) {
	file := "/var/tmp/testFile"
	os.Create(file)
	defer os.Remove(file)

	copyDir := "/var/tmp/manageLog/"
	//defer os.RemoveAll(copyDir)

	CurUser, _ := user.Current()
	CurUID, _ := strconv.Atoi(CurUser.Uid)
	CurGID, _ := strconv.Atoi(CurUser.Gid)
	job := &Job{
		UID:          CurUID,
		GID:          CurGID,
		RetainLogs:   true,
		CopyLogsDir:  copyDir + "/copyOne",
		PrintWarning: PrintErr,
		curUser:      CurUser,
	}

	job.manageLog(file)
	assert.True(t, (fileExists(file)))
	assert.True(t, (fileExists(copyDir + "/copyOne/testFile")))

	job.CopyLogsDir = ""
	job.manageLog(file)
	assert.True(t, (fileExists(file)))

	job.RetainLogs = false
	job.CopyLogsDir = copyDir + "/copyTwo"
	job.manageLog(copyDir + "/copyOne/testFile")
	assert.True(t, (fileExists(copyDir + "/copyTwo/testFile")))
	assert.False(t, (fileExists(copyDir + "/copyOne/testFile")))

	job.CopyLogsDir = ""
	job.manageLog(file)
	assert.False(t, (fileExists(file)))
}
