#!/usr/bin/env python3

# system
import argparse
import json
import os
import os.path
import sys

class MockSlurm:
    def __init__(self):
        self.location = os.path.dirname(os.path.realpath(__file__))
        self.slurmfile = self.location + "/slurmdb.json"
        self.createDBFile()
        self.slurmdb = self.openDB()
        self.setup_parsers()
        self.get_args()


    def setup_parsers(self):
        self.parser = argparse.ArgumentParser(description='Mock Slurm')
        self.cmd_parser = self.parser.add_subparsers(title="commands")

        clean_parser = self.cmd_parser.add_parser("clean",
                                                  description="clean mock_slrum",
                                                  help="Remove artifacts created by mock_slurm")
        clean_parser.set_defaults(func=self.clean)

        # scancel parser
        scancel_parser = self.cmd_parser.add_parser("scancel",
                                                    description="cancel job",
                                                    help="Cancel running job")
        scancel_parser.add_argument("jobid", help="Job ID to cancel")
        scancel_parser.set_defaults(func=self.scancel)

        # sacct parser
        sacct_parser = self.cmd_parser.add_parser("sacct",
                                             description="slurm accounting",
                                             help="Get slurm accounting information")
        sacct_parser.add_argument("--noheader", "-n", action="store_true")
        sacct_parser.add_argument("--jobs", "-j", type=str)
        sacct_parser.add_argument("--format", type=str)
        sacct_parser.add_argument("--allclusters", "-L", action="store_true")
        sacct_parser.add_argument("--startime", "-S", type=str)
        sacct_parser.set_defaults(func=self.sacct)
       
        # sbatch parser
        sbatch_parser = self.cmd_parser.add_parser("sbatch",
                                              description="submit job with script",
                                              help="Submit a job using the specified script")
        sbatch_parser.add_argument("--output", "-o", type=str)
        sbatch_parser.add_argument("--error", "-e", type=str)
        sbatch_parser.add_argument("--partition", "-p", type=str)
        sbatch_parser.add_argument("--nodes", "-N", type=int)
        sbatch_parser.add_argument("--hold", "-H", action="store_true")
        sbatch_parser.add_argument('script')
        sbatch_parser.set_defaults(func=self.sbatch)


    ############################################################################
    # mock_slurm scancel functions
    ############################################################################
    def scancel(self, args):
        ''' Simulate Slurm's scancel for runner testing '''
        error = self.slurmdb.get(args.jobid, {}).get('error', None)
        # Slurm provides no feedback regarding canceled job. At this time 
        # the runner does not validate a job is cancled so we will ony simulate a non-zero
        if error:
            self.scancelErr("simulated scancel error")
        exit(0)

    def scancelErr(self, err):
        print("scancel: error: {}".format(err), file=sys.stderr)
        exit(1)

    ############################################################################
    # mock_slurm sacct functions
    # Note that it is assumed that flags such as -n --format=state are provided
    # in addition only a single return state will be provided (e.g. no srun).
    # TODO: better handle -L and -S arguments to account for potential cases
    ############################################################################
    def sacct(self, args):
        ''' Simulate Slurm's sacct for runner testing '''
        state = self.getState(args.jobs)
        if self.slurmdb[args.jobs]['error']:
            print("sacct: error: simulated non-zero call", file=sys.stderr)
            exit(1)

        self.logOutput(args.jobs)

        print(state)
        if state == "PENDING":
            state = "RUNNING"
        elif state == "RUNNING":
            state = self.slurmdb[args.jobs]['target']

        # update db with new state
        self.slurmdb[args.jobs]['state'] = state
        self.updateDB()

        exit(0)

    def logOutput(self, jobid):
        ''' save generic output to the appropriate log file '''
        # TODO: expand logic to better facilitate testing file tailing
        pass

    def getState(self, jobid):
        '''' Return state of jobid, generate error and exit status of 1 if job not found '''
        if self.slurmdb.get(jobid):
            return self.slurmdb[jobid]['state']
        else:
            self.mockSlurmErr("jobid {} not found".format(jobid))

    ############################################################################
    # mock_slurm sbatch functions
    # Sbatch requires a valid (existing) partition when a job is submitted, each 
    # relates to a jobid and target state in the slurmdb.
    # TODO: expand support for additional arguments
    # TODO: support scripts (currently ignored)
    ############################################################################
    def sbatch(self, args):
        ''' Simulate Slurm's sbatch for runner testing '''
        jobid = self.partition(args.partition)
        if self.slurmdb[jobid]['error']:
            self.sbatchErr("simulated non-zero call")
        
        # update db for job
        self.slurmdb[jobid]['output'] = args.output.replace("%j", jobid)
        self.slurmdb[jobid]['state'] = "PENDING"

        self.generateLog(jobid)
        self.updateDB()


    def partition(self, part):
        for e in self.slurmdb:
            if self.slurmdb[e]['partition'] == part:
                return e
        self.mockSlurmErr("partition {} not found".format(part))


    def generateLog(self, jobid):
        ''' generate an empty log file based upon job's defined output '''
        if os.path.exists(self.slurmdb[jobid]['output']):
            os.remove(self.slurmdb[jobid]['output'])
        f  = open(self.slurmdb[jobid]['output'], "w+")
        f.close()

    def sbatchErr(self, err):
        print("sbatch: error: {}".format(err), file=sys.stderr)
        exit(1)
    ############################################################################
    # mock_slurm support functions
    ############################################################################
    def clean(self):
        ''' remove any traces of mock_slurm '''
        if os.path.exists(self.slurmfile):
            os.remove(self.slurmfile)

    def get_args(self):
        args = self.parser.parse_args()
        args.func(args)

    def mockSlurmErr(self, err):
        ''' print error message via stderr and exit status 1 '''
        print("mock_slurm error: {}".format(err), file=sys.stderr)
        exit(1)

    def openDB(self):
        ''' Return dict from mock slurm json (slurmfile) '''
        with open(self.slurmfile) as file:
            return json.load(file)

    def updateDB(self):
        ''' Complete replace mock slurm json (slurmfile) using current slurmdb '''
        os.remove(self.slurmfile)
        with open(self.slurmfile, 'w') as file:
            json.dump(self.slurmdb, file)

    def createDBFile(self):
        """ If mock slurm json (slurmfile) does not exist generate with default data 
        
        Default data structure is:
            <jobid> {
                id: "integer version of jobid (in case required)"
                state: "current state of the job"
                target: "target final state of the job"
                partition: "partition to use in sbatch to return job"
                error: "if the job will return an exit(1), simulate failed slurm command"
                output: "location for job's output log"
            }

        Note that the partion should be unique; however, it is not enforced. Keeping it as such 
        will allow for tests to target specific states Slurm might be in.
        """
        if not os.path.exists(self.slurmfile):
            data = {}
            data['100'] = {
                'id': 100,
                'state': 'COMPLETED',
                'target': 'COMPLETED',
                'partition': 'completed',
                'error': False,
                'output': ''
            }
            data['200'] = {
                'id': 200,
                'state': 'CANCELLED',
                'target': 'CANCELLED',
                'partition': 'cancelled',
                'error': False,
                'output': ''
            }
            data['300'] = {
                'id': 300,
                'state': 'PENDING',
                'target': 'FAILED',
                'partition': 'failed',
                'error': False,
                'output': ''
            }
            data['400'] = {
                'id': 400,
                'state': 'PENDING',
                'target': 'COMPLETED',
                'partition': 'pending',
                'error': False,
                'output': ''
            }
            data['500'] = {
                'id': 500,
                'state': 'RUNNING',
                'target': 'COMPLETED',
                'partition': 'running',
                'error': False,
                'output': ''
            }
            data['600'] = {  # Job will always produce a non-zero exit status
                'id': 600,
                'state': 'FAILED',
                'target': 'FAILED',
                'partition': 'error',
                'error': True,
                'output': ''
            }
            data['700'] = {
                'id': 700,
                'state': 'PENDING',
                'target': 'COMPLETING',
                'partition': 'completing',
                'error': False,
                'output': ''
            }
            data['999'] = {
                'id': 999,
                'state': '',
                'target': '',
                'partition': 'none',
                'error': False,
                'output': ''
            }
            with open(self.slurmfile, 'w') as f:
                json.dump(data, f)

if __name__== "__main__":
    MockSlurm()
