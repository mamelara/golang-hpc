# Mock Slurm

In an attempt to offer some level of acceptable tier 0 testing for the library the `mock_slurm.py` has been generated. These scripts attempt to mock basic Slurm behaviors the library will require. Please note the logic is incredibly simple and is only meant to capture functionality and edge cases that can only be easily simulated.

## Usage

Currently the logic to properly use the scheduler can also be found in the `.gitlab-ci.yml`. Keep in mind that since by default the runner will create a new login shell for the user before executing any command this will need accounted for in including the `test/schedulers/slurm_generic` folder on your `PATH`.

```bash
$ echo "PATH=${CI_PROJECT_DIR}/test/schedulers/slurm_generic:${PATH}" >> ~/.bashrc
$ PATH=${CI_PROJECT_DIR}/test/schedulers/slurm_generic:${PATH} go test -coverprofile=coverage.out -v -run $(cat slurm_test.go | sed -n 's/func.*\(Test.*\)(.*/\1/p' | xargs | sed 's/ /|/g')
$ sed -i '$d' ~/.bashrc
```

The use of `sed` is to ensure we only run tests relating to Slrum and avoid potentially failing tests due to this modified `PATH`. For example it would look something like `-run Test_sacctState|Test_sbatchJobID|...`.

If writing a test case and you would like to leverage you will need to first insure that the above information on adding the `test/schedulers/slurm_generic` to your path defined.

1. In the test code check if the `mock_slurm.py` is available on the path.

```go
_, err := exec.LookPath("mock_slurm.py")
if err != nil {
    t.Skip("mock_slurm.py not found")
}
```

2. Clean the mock database (json file) to avoid having a state corrupted by previous tests. This can be done by calling `cleanMockSlurm()`.

3. Write tests cases that respect the default data and logic of the "Slurm jobs". Review the strucute to better understand and defaults can be found in the `mock_slurm.py` `def createDBFile(self` function.

    * `scancel` - Will only fail on simulated error and no job id.
    * `sbatch` - Job must be submitted against a unique `--partition`, the json file is updated with a new "PENDING" state and appropriate output file is generated and logged. Will only fail on simulated error and invalid arguments.
    * `sacct` - Job status must be checked against valid exist jobid. This will transition the state of a job PENDING -> RUNNING -> target, where the target is defined in the default for each job. It will provide nominal output to the log file during this process. Will fail on simulated error and invalid job id.

## "Job" Structure

Due the nature of the logic "jobs" are managed via a json file with defined fields:

```json
<jobID>: {
    id: "integer version of jobid (in case required)"
    state: "current state of the job"
    target: "target final state of the job"
    partition: "partition to use in sbatch to return job"
    error: "if the job will return an exit(1), simulate failed slurm command"
    output: "location for job's output log"
}
```
