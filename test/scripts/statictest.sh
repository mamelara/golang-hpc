#!/bin/sh

go mod verify

# general go tests
go test -coverprofile=coverage.out -v
    
# mock Slurm specific tests
echo "PATH=${CI_PROJECT_DIR}/test/schedulers/slurm_generic:${PATH}" >> ~/.bashrc
PATH=${CI_PROJECT_DIR}/test/schedulers/slurm_generic:${PATH} \
    go test -coverprofile=coverage.out -v -run $(cat slurm_test.go | sed -n 's/func.*\(Test.*\)(.*/\1/p' | xargs | sed 's/ /|/g')
sed -i '$d' ~/.bashrc
