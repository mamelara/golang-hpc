#!/bin/sh

set -eo pipefail

docker pull codeclimate/codeclimate > /dev/null

docker run --env CODECLIMATE_CODE="$PWD" \
	--volume "$PWD":/code \
	--volume /var/run/docker.sock:/var/run/docker.sock \
	--volume /tmp/cc:/tmp/cc \
	codeclimate/codeclimate \
	analyze -f json > codeclimate.json

if [ "$(cat codeclimate.json)" != "[]" ] ; then
      apk add -U --no-cache jq > /dev/null
      jq -C . codeclimate.json
      exit 1
fi
