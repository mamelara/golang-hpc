package hpc

import (
	"fmt"
	"os"
	"testing"
	"time"
)

// generateFile creates a file using the fileName provied and writes the contents to
// said file and returns the name. Caller responsible for removing file.
func generateFile(fileName, contents string) string {
	f, _ := os.Create(fileName)
	defer f.Close()
	f.WriteString(contents)
	return f.Name()
}

func Test_cobaltExitStatus(t *testing.T) {
	eclToday := `# Example Cobalt Log
10/31/2019 21:46:33;TE;100;end=1570139193.45 location=42 task_id=2019 task_runtime=29.9651482105
10/31/2019 21:46:43;E;100;Exit_status=0 Resource_List.ncpus=1 Resource_List.nodect=1 Resource_List.walltime=0:20:00 user=test	
10/31/2019 21:46:33;TE;200;end=1570139193.45 location=42 task_id=2020 task_runtime=29.9651482105
10/31/2019 21:46:43;E;200;Exit_status=127 Resource_List.ncpus=1 Resource_List.nodect=1 Resource_List.walltime=0:20:00 user=test	
10/31/2019 21:46:33;TE;4A00;end=1570139193.45 location=42 task_id=2021 task_runtime=29.9651482105
10/31/2019 21:46:43;E;400;Exit_status=10 Resource_List.ncpus=1 Resource_List.nodect=1 Resource_List.walltime=0:20:00 user=test	
`
	eclYesterday := `# Example Cobalt Log
10/30/2019 21:46:43;E;300;Exit_status=1 Resource_List.ncpus=1 Resource_List.nodect=1 Resource_List.walltime=0:20:00 user=test	
10/30/2019 21:46:43;E;100;Exit_status=127 Resource_List.ncpus=1 Resource_List.nodect=1 Resource_List.walltime=0:20:00 user=test	
	`
	path, _ := os.Getwd()
	today := time.Now()
	yesterday := today.AddDate(0, 0, -1)
	ft := generateFile(fmt.Sprintf("%s/%d%02d%02d", path, today.Year(), today.Month(), today.Day()), eclToday)
	defer os.Remove(ft)
	fy := generateFile(fmt.Sprintf("%s/%d%02d%02d", path, yesterday.Year(), yesterday.Month(), yesterday.Day()), eclYesterday)
	defer os.Remove(fy)

	type args struct {
		logs string
		id   string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"Status 0 job", args{path, "100"}, "0", false},
		{"Status 127 job", args{path, "200"}, "127", false},
		{"Status 10 job", args{path, "400"}, "10", false},
		{"Status 1 job - previous day", args{path, "300"}, "1", false},
		{"Invalid file", args{"/var/tmp", "0"}, "", true},
		{"Invalid jobID", args{path, "101"}, "", true},
		{"No jobID", args{path, ""}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := cobaltExitStatus(tt.args.logs, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("cobaltExitStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("cobaltExitStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}
