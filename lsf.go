package hpc

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"regexp"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
)

type LSFJob struct {
	*Job
	batchCommand string
	args         []string
	jobID        string
}

func (j LSFJob) New(job *Job) (LSFJob, error) {
	//Create script Check for Errors
	script, err := job.buildScript()
	if err != nil {
		return LSFJob{}, err
	}

	//Assemble bash command
	execArgs := []string{"-I"}

	//Handle Native Specs
	if len(job.NativeSpecs) != 0 {
		//Defines an array of illegal arguments which will not be passed in as native specifications
		illegalArguments := []string{"-e", "-o", "-eo"}
		warnings := job.CheckIllegalParams(job.NativeSpecs, illegalArguments)
		for i := range warnings {
			job.PrintWarning(warnings[i])
		}
		execArgs = append(execArgs, job.NativeSpecs...)
	}

	execArgs = append(execArgs, script)

	return LSFJob{job, "bsub", execArgs, ""}, nil
}

func (j *LSFJob) RunJob() (builderr error, syserr error) {
	cmd := j.Job.setUID()
	bsubStdin := fmt.Sprintf("%s %s", j.batchCommand, strings.Join(j.args, " "))
	cmd.Stdin = bytes.NewBufferString(bsubStdin)

	stdout, syserr := cmd.StdoutPipe()
	if syserr != nil {
		logrus.WithFields(logrus.Fields{
			"job":   j.ParentJobID,
			"error": syserr,
		}).Warn("Error creating stdout pipe")
		return
	}

	stderr, syserr := cmd.StderrPipe()
	if syserr != nil {
		logrus.WithFields(logrus.Fields{
			"job":   j.ParentJobID,
			"error": syserr,
		}).Warn("Error creating stderr pipe")
		return
	}

	// execute bsub
	syserr = cmd.Start()
	if syserr != nil {
		logrus.WithFields(logrus.Fields{
			"job":   j.ParentJobID,
			"bsub":  bsubStdin,
			"error": syserr,
		}).Warn("Failed bsub command")
		j.PrintWarning("Failed to start bsub command")
		return
	}

	// get job id from first line of LSF output
	jobid, syserr := j.getJobID(stdout)
	if syserr != nil {
		// TODO: identify desired behavior when a jobID cannot be obtained
		logrus.WithFields(logrus.Fields{
			"job":   j.ParentJobID,
			"error": syserr,
		}).Warn("Failed to identify LSF jobid")
	}
	j.jobID = jobid

	// read stdout and stderr in parallel and print them to the parent
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		j.Job.tailPipe(stdout)
	}()

	go func() {
		defer wg.Done()
		j.Job.tailPipe(stderr)
	}()

	// force the tailPipe calls to complete first
	wg.Wait()

	// then wait on the command itself (this closes its pipes)
	err := cmd.Wait()
	if err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			builderr = err
		} else {
			syserr = err
		}
		return
	}

	return
}

// KillJob build command to kill job (scancel), j.jobID must be assigned.
// Returns system error if encountered.
func (j *LSFJob) KillJob() (err error) {
	cmd := j.Job.setUID()
	cmd.Stdin = bytes.NewBufferString(fmt.Sprintf("bkill %s", j.jobID))
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("cannot kill job %s: %v", j.jobID, err)
	}
	return
}

func (j *LSFJob) getJobID(pipe io.ReadCloser) (string, error) {
	jobidRe := regexp.MustCompile(`Job <(\d+)>`)

	scanner := bufio.NewScanner(pipe)
	scanner.Scan()
	line := scanner.Text()
	j.PrintToParent(line)

	matches := jobidRe.FindStringSubmatch(line)
	if len(matches) == 0 {
		return "", errors.New("cannot obtain JobID from first line of LSF output")
	}

	return matches[1], nil
}
