module gitlab.com/ecp-ci/golang-hpc

go 1.12

require (
	github.com/radovskyb/watcher v1.0.7
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
)
